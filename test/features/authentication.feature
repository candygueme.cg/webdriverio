Feature: Authentication
  Scenario: User logs in with correct credentials
    Given I open up the application
    When I fill in login as "smolnar" and password as "password123"
    Then I should be logged in as "smolnar"

