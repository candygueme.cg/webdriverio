module.exports = function () {

    Given('I open up the application', function () {
        browser
            .url('/');

    });



    When('I fill in login as {string} and password as {string}', function (login, password) {
        browser
            .waitForExist('#login')
            .setValue('#login', login)
            .setValue('#password', password)
            .click('#login-button')

    });


    Then('I should be logged in as {string}', function (string) {
        browser
            .waitForExist('#logged-in-user')
            .getText('#logged-in-user').then(function (text) {
                expect(login).to.eql(text);
            })
    });
};