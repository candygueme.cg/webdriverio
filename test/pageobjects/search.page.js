import Page  from '../pageobjects/page';

class SearchPage extends Page {

    get inputSearch() {
        return $('#gh-ac');
    }

    get btnSearch() {
        return $('#gh-btn');
    }
    get menuFashion(){
        return $('div.hl-cat-nav a[href*="Fashion"]:first-child');
    }
    open() {
       super.open('https://co.ebay.com/');
    }

    search() {
        this.inputSearch.setValue('Laptop');
        this.btnSearch.click();
    }

    getMenu(){

        this.menuFashion;
    }

}

module.exports = new SearchPage();