import {expect as chaiExpect} from 'chai';
describe('test chai', () => {
    it('assertions empty', () => {
        browser.url('https://co.ebay.com/b/Fashion/bn_7000259856');
        const banner= $('.b-promobanner__info-title');
        banner.getText();
        chaiExpect(banner).to.not.be.empty;
    });
});