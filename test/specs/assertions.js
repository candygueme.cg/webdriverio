/*describe('Watches page', () => {

    it('Should show the banner container', () => {
        browser.url('https://co.ebay.com/b/Fashion/bn_7000259856');
        const banner= $('section.b-promobanner');
        expect(banner).toBeVisible();
    });

    it('should show the banner title', () => {
        const banner= $('.b-promobanner__info-title');
        expect(banner).toHaveTextContaining('Aquí es "fashion week" todo el año');
    });

    it('should show the link of button', () => {
        const btnBanner= $('.b-promobanner__info-btn');
        expect(btnBanner).toHaveLink('https://www.ebay.com/globaldeals/moda');
        expect(btnBanner).toBeClickable();
    });
    
});